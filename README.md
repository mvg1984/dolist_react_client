## Description

React client for <a href='https://gitlab.com/mvg1984/dolist_laravel_backend'>PHP Laravel Todo API</a>

## Project setup
```
npm install
```

## Project settings
Set endpoint url and application url in the constants.js

Frontend supports browser push notifications via pusher.com. You can set pusher credentials also in the constants.js.

## Compiles and hot-reloads for development
```
npm start
```

## Compiles and minifies for production
```
npm run build
```
