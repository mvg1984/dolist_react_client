import { SET_PROFILE, SET_TOKEN } from './../actions/types';

const initialState = {
    profile: null,
    token: null
};

const profileReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_PROFILE:
      return {
        ...state,
        profile: action.payload
      };
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    default:
      return state;
  }
}

export default profileReducer;