import { SET_PROFILE, SET_TOKEN } from './types';

export const setProfile = (payload) => {
    return {
        type: SET_PROFILE,
        payload: payload
    }
}
   
export const setToken = (payload) => {
    return {
        type: SET_TOKEN,
        payload: payload
    }
}
   