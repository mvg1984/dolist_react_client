import { setProfile, setToken } from './profile';

export const mapStateToProps = state => {
    return {
        token: state.profile.token,
        profile: state.profile.profile,
    }
}

export const mapDispatchToProps = dispatch => {
    return {
        setProfile: (payload) => {
            return new Promise((resolve, reject) => {
                dispatch(setProfile(payload));
                resolve();
            }); 
        },
        setToken: (payload) => {
            return new Promise((resolve, reject) => {
                dispatch(setToken(payload));
                resolve();
            });            
        },        
    }
}