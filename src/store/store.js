import { createStore, combineReducers } from 'redux';
import profileReducer from './../reducers/profile';

const rootReducer = combineReducers({
  profile: profileReducer,
});

const configureStore = () => {
  return createStore(rootReducer);
}

export default configureStore;