// Api
import api from '../services/api';

const user = {
    profile: async function(token, props) {
        let response = await api.profile(token);

        if (!response.result) {
            localStorage.token = null;
            return null;
        }

        localStorage.token = token;
        await props.setToken(token);
        await props.setProfile(response.data);

        return response.data;
    },

    logout: async function(props) {
        localStorage.token = null;
        await props.setToken(null);
        await props.setProfile(null);            
    }
};

export default user;