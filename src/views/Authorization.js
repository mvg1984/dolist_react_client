import React, { Component } from 'react';

// Constant
import constants from '../constants';

// Utils
import users from '../utils/users';

// Error message
import ErrorMessage from '../components/ErrorMessage';

// Api
import api from '../services/api';

// Loader
import LoadingOverlay from 'react-loading-overlay';
import Spinner from 'react-spinners/BarLoader';

// Global store
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../actions/dispatcher';

// Boostrap components
import { Container, Row, Col, Button, Card, Form } from 'react-bootstrap';

class Authorization extends Component {
    // Initial state
    state = { 
        error: '',
        loading: false,
        model: {
            email: '',
            password: '',
        }
    };

    // Component has been mounted
    componentDidMount () {
        document.title = `Authorization - ${ constants.project }`;
        this.autoAuthorization();
    };   
    
    // Component will be mounted
    autoAuthorization = () => {
        this.toggleLoader(true);

        // Try to auto-login if we have token
        users.profile(localStorage.token, this.props).then((profile) => {
            if (profile) {
                this.props.history.push("/todos");
            }
        });   

        this.toggleLoader(false);
    };       

    // Show/hide loader
    toggleLoader = (state) => {
        this.setState({ loading: state });
    };

    // Handle controls changind
    handleChange = (event) => {
        this.setState({
            model: {...this.state.model, [event.target.name]: event.target.value}
        });
    };

    // Authorization
    authorization = async () => {
        this.toggleLoader(true);

        let response = await api.authorization(this.state.model);            
        if (response.result) {
            users.profile(response.data, this.props).then((profile) => {
                if (profile) {
                    this.props.history.push("/todos");
                }
            });          
        }
        else {
            this.setState({ error: response.data });
        }        

        this.toggleLoader(false);
    };

    // Render component
    render () {
        const { model, error, loading } = this.state;

        return (
            <Container>
                { loading && <LoadingOverlay active={ loading } spinner={<Spinner />} text='' /> }
                <Row className="row justify-content-center align-items-center" style={{height: '100vh'}}>
                    <Col className="col-12 col-lg-4 col-md-6 col-sm-8">
                        { error !== '' && <ErrorMessage message={ error } />}
                        <Card>
                            <Card.Header>
                                Authorization
                            </Card.Header>
                            <Card.Body>
                                <Form>
                                    <Form.Group>
                                        <Form.Control value={ model.email } onChange={ this.handleChange } name="email" type="text" placeholder="E-mail" />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control value={ model.password } onChange={ this.handleChange } name="password" type="password" placeholder="Password" />
                                    </Form.Group>
                                    <Button variant="primary" onClick={ this.authorization }>Log in</Button>   
                                </Form>         
                            </Card.Body>
                        </Card>
                        <Row className="row justify-content-center align-items-center mt-2">
                            <a href="http://127.0.0.1:1122/registration">Create account</a>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);