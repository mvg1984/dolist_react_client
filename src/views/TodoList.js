import React, { Component } from 'react';

// Constant
import constants from '../constants';

// Utils
import users from '../utils/users';

// Error message
import ErrorMessage from '../components/ErrorMessage';

// Header
import Header from '../components/Header';

// TodoRow
import TodoRow from '../components/TodoRow';

// Loader
import LoadingOverlay from 'react-loading-overlay';
import Spinner from 'react-spinners/BarLoader';

// Api
import api from '../services/api';

// Global store
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../actions/dispatcher';

// Boostrap components
import { Container, Row, Col, Button, Card, Table } from 'react-bootstrap';

class TodoList extends Component {
    // Initial state
    state = { 
        ready: true,
        loading: false,
        error: '',
        profile: null,
        list: [],
    };

    // Component has been mounted
    componentDidMount () {
        document.title = `Todo list - ${ constants.project }`;

        // If user not authorized, redirect to the authorization
        users.profile(localStorage.token, this.props).then((profile) => {
            if (!profile) {
                this.props.history.push("/");
            }
            else {
                this.setState({
                    ready: true,
                    profile: profile
                });

                this.todoList();
            }
        });          
    };    

    // Show/hide loader
    toggleLoader = (state) => {
        this.setState({ loading: state });
    };

    // Load todo list
    todoList = async () => {
        this.toggleLoader(true);

        let response = await api.todoList(this.props.token);
        if (response.result) {
            this.setState({ 
                list: response.data,
                error: ''
            });
        }
        else {
            this.setState({ error: response.data });
        }        

        this.toggleLoader(false);
    };

    // Remove todo
    todoDelete = async (item) => {
        let { list } = this.state;

        if (window.confirm('Are you sure?')) {
            list.splice(list.indexOf(item), 1);
            this.setState({ list });
            await api.todoDelete(this.props.token, item.id);
        }           
    };    

    // Render component
    render () {
        const { ready, loading, error, list } = this.state;
        const { profile } = this.props;

        return (
            <Container>
                { loading && <LoadingOverlay active={ loading } spinner={<Spinner />} text='' /> }

                <Header profile={ profile } props={ this.props } />

                { ready && <div>
                    <Row className="mt-3">
                        <Col className="col-12">
                            <Button variant="primary" className="float-right">Create new todo</Button>
                        </Col>
                    </Row>

                    { error !== '' && <Row className="mt-3">
                        <Col className="col-12">
                            <ErrorMessage message={ error } />
                        </Col>
                    </Row>}

                    <Row className="mt-3">
                        <Col className="col-12">  
                            <Card>
                                <Card.Header>
                                    Todo list
                                </Card.Header>
                                <Card.Body className="card-table">
                                    <Table>
                                        <tbody>  
                                            { list.length === 0 && <tr><td className="text-center no-border"> There are no todos yet</td></tr> } 

                                            { list.map(todo => {
                                                return (
                                                    <TodoRow 
                                                        key={ todo.id } 
                                                        todo={ todo } 
                                                        onDelete={() => { this.todoDelete(todo) }} 
                                                    />
                                                );
                                            }) }
                                        </tbody>
                                    </Table>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>  
                </div>}               
            </Container>
        );
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);