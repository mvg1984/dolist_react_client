import React, { Component } from 'react';

// Constant
import constants from '../constants';

// Utils
import users from '../utils/users';

// Error message
import ErrorMessage from '../components/ErrorMessage';

// Header
import Header from '../components/Header';

// Loader
import LoadingOverlay from 'react-loading-overlay';
import Spinner from 'react-spinners/BarLoader';

// Api
import api from '../services/api';

// Router
import { Link } from "react-router-dom";

// Moment
import Moment from 'react-moment';

// Global store
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../actions/dispatcher';

// Boostrap components
import { Container, Row, Col, Button, Card, Table, Badge } from 'react-bootstrap';

class TodoItem extends Component {
    // Initial state
    state = { 
        ready: true,
        loading: false,
        error: '',
        profile: null,
        id: null,
        colors: [],
        groups: [],
        importances: [],
        useDeadline: false,
        useReminder: false,        
        files: [],
        file: null,        
        model: {
            name: null,
            group_id: null,
            importance_id: null,
            description: null,
            deadline_at: null,
            deadline_date: null,
            deadline_time: null,
            reminder_at: null,
            reminder_date: null,
            reminder_time: null,
            colors_id: [],
        }
    };

    // Component has been mounted
    componentDidMount () {
        document.title = `Todo item - ${ constants.project }`;

        // If user not authorized, redirect to the authorization
        users.profile(localStorage.token, this.props).then((profile) => {
            if (!profile) {
                this.props.history.push("/");
            }
            else {
                this.setState({
                    profile: profile
                }, async () => {
                    if (this.props.match.params.id) {
                        this.toggleLoader(true);

                        // Check, if we have requested todo on server
                        let response = await api.todo(this.props.token, this.props.match.params.id);
                        if (response.result) {
                            this.setState({  
                                id: this.props.match.params.id,
                                model: response.data,
                            });
                        }
                        else {
                            this.props.history.replace("/404");
                        }

                        this.toggleLoader(false);
                    }

                    this.setState({ ready: true });
                });
            }
        }); 
    };    

    // Show/hide loader
    toggleLoader = (state) => {
        this.setState({ loading: state });
    };

    // Go back to the todo list
    back = () => {
        this.props.history.push("/todos");
    };
  
    // Render component
    render () {
        const { ready, loading, error } = this.state;
        const { profile } = this.props;

        return (
            <Container>
                { loading && <LoadingOverlay active={ loading } spinner={<Spinner />} text='' /> }

                <Header profile={ profile } props={ this.props } />

                { ready && <div>
                    <Row className="mt-3">
                        <Col className="col-6">
                            <Button variant="link" onClick={ this.back }>Back to the list</Button>
                        </Col> 
                        <Col className="col-6">
                            <Button variant="primary" className="float-right">Save</Button>
                        </Col>
                    </Row>

                    { error !== '' && <Row className="mt-3">
                        <Col className="col-12">
                            <ErrorMessage message={ error } />
                        </Col>
                    </Row>}                    
                </div>}               
            </Container>
        );
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);