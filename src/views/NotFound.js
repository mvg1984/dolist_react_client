import React, { Component } from 'react';

// Constant
import constants from '../constants';

// Boostrap components
import { Container, Row } from 'react-bootstrap';

class NotFound extends Component {
    // Component has been mounted
    componentDidMount () {
        document.title = `404 - ${ constants.project }`;
    };    

    // Render component
    render () {
        return (
            <Container>
                <Row className="justify-content-center align-items-center" style={{ 'height': '100vh' }}>
                    404: Page not found
                </Row> 
            </Container>
        );
    };
}

export default NotFound;