import React from 'react';

// Router
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Views
import Authorization from './Authorization';
import TodoList from './TodoList';
import TodoItem from './TodoItem';
import NotFound from './NotFound';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/css/styles.css';

function App() {
	return (
		<Router>
			<Switch>
				<Route exact name="index" path="/" component={ Authorization } /> 
				<Route exact name="todos" path="/todos" component={ TodoList } /> 
				<Route exact name="todo" path="/todos/:id" component={ TodoItem } /> 
				<Route path="*" component={ NotFound } />
			</Switch>
		</Router>
	);
}

export default App; 