import React from 'react';

// Utils
import users from '../utils/users';

// Boostrap components
import { Row, Col, Button } from 'react-bootstrap';

const Header = ({ profile, props }) => {
    return (
        <div>
            <Row className="row mt-3">
                <Col className="col-6">
                    Hello, <a href="/">{ profile ? ` ${ profile.name }` : '...' }</a>. This is Dolist application.
                </Col>
                <Col className="col-6">
                    <Button variant="light" onClick={() => { users.logout(props).then(() => props.history.push('/')) }} className="float-right">Log out</Button>
                </Col>
            </Row>
            <hr /> 
        </div>
    );
};

export default Header;
    