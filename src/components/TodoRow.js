import React from 'react';

// Moment
import Moment from 'react-moment';

// Boostrap components
import { Button, Badge } from 'react-bootstrap';

// Router
import { Link } from "react-router-dom";

const ErrorMessage = ({ todo, onEdit, onDelete }) => {
    return (
        <tr>
            <td>
                <Link to={`/todos/${ todo.id }`}>{ todo.name }</Link>
            </td>
            <td width="150">
                <Moment format="DD.MM.YYYY, HH:mm">{ todo.created_at }</Moment>                                                    
            </td>
            <td width="70" className="d-none d-lg-table-cell">
                { todo.group.name }
            </td>
            <td width="90" className="d-none d-lg-table-cell">
                { todo.colors.map(color => <Badge key={ color.id } style={{ 'backgroundColor': color.code }}>&nbsp;</Badge>)}
            </td>
            <td width="100">
                <Button onClick={ onDelete } variant="danger" size="sm" className="btn-sm float-right">Remove</Button>
            </td>
        </tr>
    );
};

export default ErrorMessage;
    